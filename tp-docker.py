import bottle
from raven import Client
from raven.contrib.bottle import Sentry

counter = 0 #Initialise la valeur du compteur a 0

@bottle.get('/ping') #Creation de la route '/ping'
def ping():
    global counter #On recupere la variable globale counter
    counter += 1 #On incremente le compteur
    return { "message" : 'pong' } #On renvoi le message 'pong' sous forme de json

@bottle.get('/count') #Creation de la route '/count'
def count():
    global counter #On recupere la variable globale counter
    return { "pingCount" : counter } #On renvoi la valeur du compteur sous forme de json

app = bottle.app() #On cree une instance de bottle
app.catchall = False #On desactive la captcha pour ne pas generer de conflit avec Sentry

client = Client('https://efcf0b62730e40cd96a63847ab5f6ee0:df98de8e3a7c4ae3a1e0a018d6417461@sentry.io/257516') #Creation du client Sentry
app = Sentry(app, client) #On cast le client sentry en application bottle

client.captureMessage('Launching application') #Message de demarrage de l'application

bottle.run(app, host='localhost', port=8080) #On lance bottle

client.captureMessage('Stopping application') #Message de fin de l'application